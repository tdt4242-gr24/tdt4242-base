async function setUser() {
  getCurrentUser().then((user) => {
    let input = document.querySelector("#username");
    input.value = user.username;
    input.disabled = true;
    input = document.querySelector("#email");
    input.value = user.email;
    input.disabled = true;
    input = document.querySelector("#phone_number");
    input.value = user.phone_number;
    input.disabled = true;
    input = document.querySelector("#country");
    input.value = user.country;
    input.disabled = true;
    input = document.querySelector("#city");
    input.value = user.city;
    input.disabled = true;
    input = document.querySelector("#street_address");
    input.value = user.street_address;
    input.disabled = true;
  });
}

function handleChangesButton() {
  btnEditAccount = document.querySelector("#btn-edit-account");
  btnConfirmChanges = document.querySelector("#btn-confirm-changes");
  btnConfirmChanges.className += " hide";
  btnEditAccount.className = btnEditAccount.className.replace(" hide", "");
  setReadOnly(true, "#form-register-user");
  document.querySelector("#username").readOnly = true;
  updateUser();
}

function handleEditButton() {
  btnEditAccount = document.querySelector("#btn-edit-account");
  btnConfirmChanges = document.querySelector("#btn-confirm-changes");
  btnEditAccount.className += " hide";
  btnConfirmChanges.className = btnConfirmChanges.className.replace(
    " hide",
    ""
  );

  setReadOnly(false, "#form-register-user");
  document.querySelector("#username").readOnly = true;
}

async function updateUser() {
  let form = document.querySelector("#form-register-user");
  let formData = new FormData(form);

  let body = {
    email: formData.get("email"),
    phone_number: formData.get("phone_number"),
    country: formData.get("country"),
    city: formData.get("city"),
    street_address: formData.get("street_address"),
  };
  user = await getCurrentUser();
  response = await sendRequest("PATCH", `${HOST}/api/users/${user.id}/`, body);
  if (response.ok) {
  } else {
  }
  window.location.replace("mypage.html");
}

async function changePassword() {
  let form = document.querySelector("#form-change-password");
  let formData = new FormData(form);

  let body = {
    old_password: formData.get("old_password"),
    password: formData.get("password"),
    password2: formData.get("password2"),
  };
  user = await getCurrentUser();
  response = await sendRequest(
    "PUT",
    `${HOST}/api/change_password/${user.id}/`,
    body
  );
  if (response.ok) {
    form.reset();
  } else {
    console.log(response.data);
  }
}

window.addEventListener("DOMContentLoaded", async () => {
  setUser();

  btnEditAccount = document.querySelector("#btn-edit-account");
  btnEditAccount.addEventListener("click", handleEditButton);
  btnConfirmChanges = document.querySelector("#btn-confirm-changes");
  btnConfirmChanges.addEventListener("click", handleChangesButton);
  btnChangePassword = document.querySelector("#btn-change-password");
  btnChangePassword.addEventListener("click", changePassword);
});
