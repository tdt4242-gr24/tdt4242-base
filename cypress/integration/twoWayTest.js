// Test are based on that all fields on the register page should be field in.

describe("2-way test", () => {
  beforeEach(() => {
    cy.visit("register.html");
  });

  it("Should not fail, id 1", () => {
    cy.get('input[name="username"]').type("Username1");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("12345678");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });

  it("Should fail, 128", () => {
    cy.get('input[name="password1"]').type("password");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });

  it("Should fail, id 136", () => {
    cy.get('input[name="phone_number"]').type("12345678");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });

  it("Should fail, id 249", () => {
    cy.get('input[name="username"]').type("Username1");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });

  it("Should fail, id 26", () => {
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });

  it("Should fail, id 99", () => {
    cy.get('input[name="username"]').type("Username1");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("12345678");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });

  it("Should fail, id 49", () => {
    cy.get('input[name="username"]').type("Username1");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });

  it("Should fail, id", () => {
    cy.get('input[name="username"]').type("Username1");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});
