describe("Test FR 01-02", () => {
  before(() => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("testuser9");
    cy.get('input[name="password"]').type("1");
    cy.get('input[name="password1"]').type("1");
    cy.get('input[id="btn-create-account"]').click();
  });
  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser9");
    cy.get('input[name="password"]').type("1");
    cy.get('input[id="btn-login"]').click();
  });

  it("Test if rest time field exist", () => {
    cy.get('input[id="btn-create-workout"]').should("be.visible").click();
    cy.get('input[name="resttime"]');
  });

  it("Test choose intensity", () => {
    cy.get('input[id="btn-create-workout"]').should("be.visible").click();
    cy.get('select[name="intensity"]').select("Low");
    cy.get('select[name="intensity"]').select("Medium");
    cy.get('select[name="intensity"]').select("High");
  });
});

describe("Test FR 03-05", () => {
  before(() => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("testuser9");
    cy.get('input[name="password"]').type("1");
    cy.get('input[name="password1"]').type("1");
    cy.get('input[id="btn-create-account"]').click();
  });
  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser9");
    cy.get('input[name="password"]').type("1");
    cy.get('input[id="btn-login"]').click();
  });

  it("Test to create an workout and check if the values are right when ", () => {
    const workoutName = "ThisIsWorkout";
    cy.get('input[id="btn-create-workout"]').should("be.visible").click();
    cy.get('input[name="name"]').type(workoutName);
    cy.get('input[name="date"]').type("2022-12-12T20:20");
    cy.get('textArea[name="notes"]').type("Notes");
    cy.get('input[name="sets"]').type("1");
    cy.get('input[name="number"]').type("1");
    cy.get('input[name="resttime"]').type("1");
    cy.get('select[name="type"]').select("Super");
    cy.get('input[id="btn-ok-workout"]').click();
    cy.get("h5")
      .contains(workoutName)
      .parent()
      .parent()
      .within(() => {
        cy.get("table")
          .children()
          .within(() => {
            cy.get("tr").contains("Duration").parent().contains("11");
            cy.get("tr").contains("Calories").parent().contains("100");
            cy.get("tr").contains("Muscle Groups").parent().contains("Back");
            cy.get("tr").contains("Intensity").parent().contains("LOW");
          });
      });
  });
});

describe("Test FR 06", () => {
  before(() => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("testuser9");
    cy.get('input[name="password"]').type("1");
    cy.get('input[name="password1"]').type("1");
    cy.get('input[id="btn-create-account"]').click();
  });
  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser9");
    cy.get('input[name="password"]').type("1");
    cy.get('input[id="btn-login"]').click();
  });

  it("Test if rest time field exist", () => {
    cy.visit("workouts.html");
    cy.get(".navbar-brand").click();
    cy.url().should("eq", "http://localhost:3000/index.html");
  });
});

describe("Test FR 07-09", () => {
  before(() => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("testuser10");
    cy.get('input[name="password"]').type("1");
    cy.get('input[name="password1"]').type("1");
    cy.get('input[id="btn-create-account"]').click();
  });

  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser10");
    cy.get('input[name="password"]').type("1");
    cy.get('input[id="btn-login"]').click();
  });

  it("Test if changing personal info works", () => {
    const email = "test@test.test";
    const phone_number = 12345678;
    const country = "norway";
    const city = "trondheim";
    const street_address = "NTNU";
    cy.get('a[id="nav-mypage"]').should("be.visible").click();
    cy.wait(150);
    cy.get('input[id="btn-edit-account"]')
      .should("be.visible")
      .click({ force: true });
    cy.get('input[id="btn-edit-account"]').should("not.be.visible");
    cy.get('input[name="email"]').should("not.be.disabled").clear().type(email);
    cy.get('input[name="phone_number"]')
      .should("not.be.disabled")
      .clear()
      .type(phone_number);
    cy.get('input[name="country"]')
      .should("not.be.disabled")
      .clear()
      .type(country);
    cy.get('input[name="city"]').should("not.be.disabled").clear().type(city);
    cy.get('input[name="street_address"]')
      .should("not.be.disabled")
      .clear()
      .type(street_address);
    cy.get('input[id="btn-confirm-changes"]').should("not.be.disabled").click();
    cy.reload(false);
    cy.wait(150);
    cy.get('input[name="email"]').should("have.value", email);
    cy.get('input[name="phone_number"]').should("have.value", phone_number);
    cy.get('input[name="country"]').should("have.value", country);
    cy.get('input[name="city"]').should("have.value", city);
    cy.get('input[name="street_address"]').should("have.value", street_address);
  });

  it("Test change password", () => {
    cy.get('a[id="nav-mypage"]').should("be.visible").click();
    cy.wait(150);
    const oldPassword = "1";
    const newPassword = "2";

    cy.get('input[name="old_password"]')
      .should("not.be.disabled")
      .clear()
      .type(oldPassword);
    cy.get('input[name="password"]')
      .should("not.be.disabled")
      .clear()
      .type(newPassword);
    cy.get('input[name="password2"]')
      .should("not.be.disabled")
      .clear()
      .type(newPassword);
    cy.get('input[id="btn-change-password"]').should("not.be.disabled").click();
    cy.visit("login.html");
    cy.reload(true);
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser10");
    cy.get('input[name="password"]').type(newPassword);
    cy.get('input[id="btn-login"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
});
