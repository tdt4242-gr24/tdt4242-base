//Workouts and users are added to the initial testData.

describe("Check if athlete can accsess all his workout", () => {
  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("Athlete");
    cy.get('input[name="password"]').type("1234");
    cy.get('input[id="btn-login"]').click();
    cy.wait(150);
  });
  it("Athlete should have access to his own private workout", () => {
    cy.get("h5").contains("Private");
  });
  it("Athlete should have access to his own coach workout", () => {
    cy.get("h5").contains("Coach");
  });
  it("Athlete should have access to his own public workout", () => {
    cy.get("h5").contains("Public");
  });
});

describe("Check if coach can accsess all his workout", () => {
  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("Coach");
    cy.get('input[name="password"]').type("1234");
    cy.get('input[id="btn-login"]').click();
    cy.wait(150);
  });
  it("Coach should not have access to his athlets private workout", () => {
    cy.get("h5").contains("Private").should("not.exist");
  });
  it("Coach should have access to his athlets coach workout", () => {
    cy.get("h5").contains("Coach");
  });
  it("Coach should have access to his athlets public workout", () => {
    cy.get("h5").contains("Public");
  });
});

describe("Check if visitor can access all his workout", () => {
  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("Visitor");
    cy.get('input[name="password"]').type("1234");
    cy.get('input[id="btn-login"]').click();
    cy.wait(150);
  });
  it("Visitor should not have access to athletes private workouts", () => {
    cy.get("h5").contains("Private").should("not.exist");
  });
  it("Visitor should not have access to athletes coach workouts", () => {
    cy.get("h5").contains("Coach").should("not.exist");
  });
  it("Visitor should have access to athletes public workouts", () => {
    cy.get("h5").contains("Public");
  });
});
