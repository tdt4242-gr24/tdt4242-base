describe("Test username length", () => {
  it("Should fail if username is less than 4 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("abc");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if username is more than 3 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("aaaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if username is more than 3 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("bbbbb");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if username is more than 3 and less then 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("aaaaaaaaaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if username is more than 3 and less then 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("aaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if username is more than 3 and less then 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("aaaaaaaaaaaaaaaaaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should fail if username is more than 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("aaaaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});

describe("Test email length", () => {
  it("Should fail if email is less than 4 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("abce");
    cy.get('input[name="email"]').type("fem");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if email is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("cccc");
    cy.get('input[name="email"]').type("f@e.c");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if email is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("fffff");
    cy.get('input[name="email"]').type("f@eg.c");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if email is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("eeee");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if email is more than 4 and less then 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("dddd");
    cy.get('input[name="email"]').type(
      "fake@emailbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb.com"
    );
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if email is more than 4 and less then 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("gggg");
    cy.get('input[name="email"]').type(
      "fake@emailbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb.com"
    );
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should fail if e-mail is more than 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("hhhhh");
    cy.get('input[name="email"]').type(
      "fake@emailbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb.com"
    );
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});

describe("Test password length", () => {
  it("Should fail if password is less than 4 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("3aaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("123");
    cy.get('input[name="password1"]').type("123");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if password is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("3bbbb");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("1234");
    cy.get('input[name="password1"]').type("1234");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if password is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("3ccc");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("12345");
    cy.get('input[name="password1"]').type("12345");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if password is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("3dddd");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type(
      "1234567891234567891234567123456789123456789123456"
    );
    cy.get('input[name="password1"]').type(
      "1234567891234567891234567123456789123456789123456"
    );
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if password is more than 4 and less then 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("3eee");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type(
      "1234567891234567891234567123456789123456789123456"
    );
    cy.get('input[name="password1"]').type(
      "1234567891234567891234567123456789123456789123456"
    );
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if password is more than 4 and less then 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("3fff");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type(
      "12345678912345678912345671234567891234567891234567"
    );
    cy.get('input[name="password1"]').type(
      "12345678912345678912345671234567891234567891234567"
    );
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should fail if password is more than 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("3gggg");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type(
      "123456789123456789123456712345678912345678912345678"
    );
    cy.get('input[name="password1"]').type(
      "123456789123456789123456712345678912345678912345678"
    );
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});

describe("Test country length", () => {
  it("Should fail if country is less than 4 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("4aaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("aaa");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if country is more than 4 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("4bbbb");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("aaaa");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if country is more than 4 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("4ccc");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("aaaaa");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if country is more than 4 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("4dddd");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("aaaaaaaaaa");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if country is more than 4 and less then 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("4eee");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("aaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if country is more than 4 and less then 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("4fff");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("aaaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should fail if country is more than 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("4gggg");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("aaaaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});

describe("Test phone number length", () => {
  it("Should fail if phone number is less than 6 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("5aaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("12345");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if phone number is more than 6 and less tham 16 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("5bbbb");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("123456");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if phone number is more than 6 and less tham 16 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("5ccc");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("1234567");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if phone number is more than 6 and less tham 16 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("5dddd");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("12345678912");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if phone number is more than 6 and less then 16 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("5eee");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("123456789123456");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if phone number is more than 6 and less then 16 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("5fff");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("1234567891234567");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should fail if phone number is more than 16 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("5gggg");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("12345678912345678");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});

describe("Test city length", () => {
  it("Should fail if city is less than 4 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("6aaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("aaa");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if city is more than 4 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("6bbbb");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("aaaa");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if city is more than 4 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("6ccc");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("aaaaa");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if city is more than 4 and less tham 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("6dddd");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("aaaaaaaaaa");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if city is more than 4 and less then 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("6eee");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("aaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if city is more than 4 and less then 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("6fff");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("aaaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should fail if city is more than 20 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("6gggg");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("aaaaaaaaaaaaaaaaaaaaa");
    cy.get('input[name="street_address"]').type("NTNU");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});

describe("Test street address length", () => {
  it("Should fail if street address is less than 4 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("7aaa");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("aaa");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
  it("Should not fail if street address is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("7bbbb");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("aaaa");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if street address is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("7ccc");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("aaaaa");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if street address is more than 4 and less tham 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("7dddd");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type("aaaaaaaaaaaaaaaaaaaaaaaaa");
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if street address is more than 4 and less then 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("7eee");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type(
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    );
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should not fail if street address is more than 4 and less then 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("7fff");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type(
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    );
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/workouts.html");
  });
  it("Should fail if street address is more than 50 characters", () => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("7gggg");
    cy.get('input[name="email"]').type("fake@email.com");
    cy.get('input[name="password"]').type("password");
    cy.get('input[name="password1"]').type("password");
    cy.get('input[name="phone_number"]').type("password");
    cy.get('input[name="country"]').type("norway");
    cy.get('input[name="city"]').type("Trondheim");
    cy.get('input[name="street_address"]').type(
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    );
    cy.get('input[id="btn-create-account"]').click();
    cy.url().should("eq", "http://localhost:3000/register.html");
  });
});
