describe("Boundary test: Calories", () => {
  before(() => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("testuser1");
    cy.get('input[name="password"]').type("1");
    cy.get('input[name="password1"]').type("1");
    cy.get('input[id="btn-create-account"]').click();
  });

  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser1");
    cy.get('input[name="password"]').type("1");
    cy.get('input[id="btn-login"]').click();
    cy.get('a[id="nav-exercises"]').should("be.visible").click();
    cy.get('input[id="btn-create-exercise"]').should("be.visible").click();
    cy.wait(150);
    cy.get('input[name="name"]').type("Test");
    cy.get('textarea[name="description"]').type("Test");
    cy.get('input[name="unit"]').type("1");
    cy.get('input[name="duration"]').type("1");
  });

  it("Should faile if calories is less than 0.", () => {
    cy.get('input[name="calories"]').type("-1");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.contains("Could not create new exercise");
    cy.contains("calories");
  });
  it("Should pass if calories is between or equal to 0 and 10000.", () => {
    cy.get('input[name="calories"]').type("0");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if calories is between or equal to 0 and 10000.", () => {
    cy.get('input[name="calories"]').type("1");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if calories is between or equal to 0 and 10000.", () => {
    cy.get('input[name="calories"]').type("5000");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if calories is between or equal to 0 and 10000.", () => {
    cy.get('input[name="calories"]').type("9999");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if calories is between or equal to 0 and 10000.", () => {
    cy.get('input[name="calories"]').type("10000");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should fail if calories is bigger than 0 and 10000.", () => {
    cy.get('input[name="calories"]').type("10001");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.contains("Could not create new exercise");
    cy.contains("calories");
  });
});

describe("Boundary test: Duration", () => {
  before(() => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("testuser2");
    cy.get('input[name="password"]').type("1");
    cy.get('input[name="password1"]').type("1");
    cy.get('input[id="btn-create-account"]').click();
  });

  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser2");
    cy.get('input[name="password"]').type("1");
    cy.get('input[id="btn-login"]').click();
    cy.get('a[id="nav-exercises"]').should("be.visible").click();
    cy.get('input[id="btn-create-exercise"]').should("be.visible").click();
    cy.get('input[name="name"]').type("Test");
    cy.get('textarea[name="description"]').type("Test");
    cy.get('input[name="unit"]').type("1");
    cy.get('input[name="calories"]').type("2");
  });

  it("Should faile if duartion is less than 0.", () => {
    cy.get('input[name="duration"]').type("-1");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.contains("Could not create new exercise");
    cy.contains("duration");
  });
  it("Should pass if duartion is between or equal to 0 and 10000.", () => {
    cy.get('input[name="duration"]').type("0");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if duartion is between or equal to 0 and 10000.", () => {
    cy.get('input[name="duration"]').type("1");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if duartion is between or equal to 0 and 10000.", () => {
    cy.get('input[name="duration"]').type("5000");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if duartion is between or equal to 0 and 10000.", () => {
    cy.get('input[name="duration"]').type("9999");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if duartion is between or equal to 0 and 10000.", () => {
    cy.get('input[name="duration"]').type("10000");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should fail if duartion is bigger than 0 and 10000.", () => {
    cy.get('input[name="duration"]').type("10001");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.contains("Could not create new exercise");
    cy.contains("duration");
  });
});

describe("Boundary test: Units", () => {
  before(() => {
    cy.visit("register.html");
    cy.get('input[name="username"]').type("testuser1");
    cy.get('input[name="password"]').type("1");
    cy.get('input[name="password1"]').type("1");
    cy.get('input[id="btn-create-account"]').click();
  });

  beforeEach(() => {
    cy.visit("login.html");
    cy.get('input[name="username"]').type("testuser1");
    cy.get('input[name="password"]').type("1");
    cy.get('input[id="btn-login"]').click();
    cy.get('a[id="nav-exercises"]').should("be.visible").click();
    cy.get('input[id="btn-create-exercise"]').should("be.visible").click();
    cy.get('input[name="name"]').type("Test");
    cy.get('textarea[name="description"]').type("Test");
    cy.get('input[name="calories"]').type("1");
    cy.get('input[name="duration"]').type("1");
  });

  it("Should faile if no unit is typed.", () => {
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.contains("Could not create new exercise");
    cy.contains("unit");
  });
  it("Should pass if unit length is between or equal to 1 and 15 characters long.", () => {
    cy.get('input[name="unit"]').type("a");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if unit length is between or equal to 1 and 15 characters long.", () => {
    cy.get('input[name="unit"]').type("aa");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if unit length is between or equal to 1 and 15 characters long.", () => {
    cy.get('input[name="unit"]').type("aaaaaaa");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if unit length is between or equal to 1 and 15 characters long.", () => {
    cy.get('input[name="unit"]').type("aaaaaaaaaaaaaa");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should pass if unit length is between or equal to 1 and 15 characters long.", () => {
    cy.get('input[name="unit"]').type("aaaaaaaaaaaaaaa");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.url().should("eq", "http://localhost:3000/exercises.html");
  });
  it("Should fail if unit length is longer than 15", () => {
    cy.get('input[name="unit"]').type("aaaaaaaaaaaaaaa");
    cy.get('input[id="btn-ok-exercise"]').click();
    cy.contains("Could not create new exercise");
    cy.contains("calories");
  });
});
