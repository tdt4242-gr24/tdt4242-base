"""
Tests for the workouts application.
"""
from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework.test import APIRequestFactory
from .models import Workout, WorkoutFile
from .permissions import *

# Create your tests here.


class WorkoutPermissionTestCase(TestCase):
    def setUp(self):

        user_values = {
            'email': 'test@test.com',
            'username': 'user',
            'password': 'password1',
            'phone_number': '',
            'country': '',
            'city': '',
            'street_address': '',
        }

        coach_values = {
            'email': 'test@test.com',
            'username': 'coach',
            'password': 'password1',
            'phone_number': '',
            'country': '',
            'city': '',
            'street_address': ''
        }

        other_values = {
            'email': 'test@test.com',
            'username': 'other',
            'password': 'password1',
            'phone_number': '',
            'country': '',
            'city': '',
            'street_address': ''
        }


        self.coach = get_user_model().objects.create_user(**coach_values)
        self.coach.save()
        self.user = get_user_model().objects.create_user(**user_values, coach=self.coach)
        self.user.save()
        self.other = get_user_model().objects.create_user(**other_values)
        self.other.save()
        self.workout_not_public = Workout(owner=self.user, date="2022-12-01 12:12")
        self.workout_not_public.save()
        self.workout = Workout(owner=self.user, date="2022-12-01 12:12", visibility="PU")
        self.workout.save()
        self.workoutFile = WorkoutFile.objects.create(owner=self.user, workout=self.workout, file="")
        self.workoutFile.save()
        
    
        self.factory = APIRequestFactory()
        self.request_user = self.factory.get('/workout/1/')
        self.request_user.user=self.user
        self.request_coach = self.factory.get('/workout/1/')
        self.request_coach.user=self.coach
        self.request_other = self.factory.get('/workout/1/')
        self.request_other.user=self.other

    def test_is_owner(self):

        self.assertTrue(IsOwner.has_object_permission(IsOwner, view ="", request=self.request_user, obj=self.workout))
        self.assertFalse(IsOwner.has_object_permission(IsOwner, view ="", request=self.request_coach, obj=self.workout))
        self.assertFalse(IsOwner.has_object_permission(IsOwner, view ="", request=self.request_other, obj=self.workout))

    def test_owner_of_workout(self):
        request_post = self.factory.post('/workout/1/', format='json')
        request_get = self.factory.get('/workout/1/', format='json')
        request_post.user=self.user
        request_get.user=self.user
        request_post.data={"workout": "/workoiut/1/"}
        
        self.assertFalse(IsOwnerOfWorkout.has_object_permission(IsOwnerOfWorkout, view ="", request=self.request_other, obj=self.workoutFile))
        self.assertFalse(IsOwnerOfWorkout.has_object_permission(IsOwnerOfWorkout, view ="", request=self.request_coach, obj=self.workoutFile))
        self.assertTrue(IsOwnerOfWorkout.has_object_permission(IsOwnerOfWorkout, view ="", request=request_get, obj=self.workoutFile))
        self.assertTrue(IsOwnerOfWorkout.has_object_permission(IsOwnerOfWorkout, view ="", request=request_post, obj=self.workoutFile))
        self.assertTrue(IsOwnerOfWorkout.has_permission(IsOwnerOfWorkout, view ="", request=request_get))
        self.assertTrue(IsOwnerOfWorkout.has_permission(IsOwnerOfWorkout, view ="", request=request_post))
        self.assertTrue(IsOwnerOfWorkout.has_permission(IsOwnerOfWorkout, view ="", request=self.request_coach))
        self.assertTrue(IsOwnerOfWorkout.has_permission(IsOwnerOfWorkout, view ="", request=self.request_other))
        request_post.data = {}
        self.assertFalse(IsOwnerOfWorkout.has_permission(IsOwnerOfWorkout, view ="", request=request_post))

    def test_is_coach_and_visible_to_coach(self):
        self.assertTrue(IsCoachAndVisibleToCoach.has_object_permission(IsCoachAndVisibleToCoach, view ="", request=self.request_coach, obj=self.workout))
        self.assertFalse(IsCoachAndVisibleToCoach.has_object_permission(IsCoachAndVisibleToCoach, view ="", request=self.request_user, obj=self.workout))
        self.assertFalse(IsCoachAndVisibleToCoach.has_object_permission(IsCoachAndVisibleToCoach, view ="", request=self.request_other, obj=self.workout))


    def test_is_coach_of_workout_and_visible_to_coach(self):
        self.assertTrue(IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(IsCoachOfWorkoutAndVisibleToCoach, view ="", request=self.request_coach, obj=self.workoutFile))
        self.assertFalse(IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(IsCoachOfWorkoutAndVisibleToCoach, view ="", request=self.request_user, obj=self.workoutFile))
        self.assertFalse(IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(IsCoachOfWorkoutAndVisibleToCoach, view ="", request=self.request_other, obj=self.workoutFile))

    def test_is_public(self):
        request = self.factory.get('/workout/1/')
        request.user=self.coach
        self.assertTrue(IsPublic.has_object_permission(IsPublic, view ="", request=self.request_other, obj=self.workout))
        self.assertFalse(IsPublic.has_object_permission(IsPublic, view ="", request=self.request_other, obj=self.workout_not_public))

    def test_is_workout_public(self):
        request = self.factory.get('/workout/1/')
        request.user=self.coach
        self.assertTrue(IsWorkoutPublic.has_object_permission(IsWorkoutPublic, view ="", request=request, obj=self.workoutFile))

    def test_is_read_only(self):
        self.assertTrue(IsReadOnly.has_object_permission(IsReadOnly, view ="", request=self.request_user, obj=self.workoutFile))