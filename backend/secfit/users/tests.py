from django.test import TestCase
from rest_framework.test import APIRequestFactory
from .serializers import UserSerializer
from .models import User

# Create your tests here.
class UserSerializerTestCase(TestCase):
    def setUp(self):
        self.user_values = {
            'email': 'test@test.com',
            'username': 'tes99566t',
            'password': 'password1',
            'phone_number': '12345678',
            'country': 'norway',
            'city': 't',
            'street_address': ''
        }
        self.factory = APIRequestFactory()
        self.request = self.factory.post('/users/', self.user_values, format='json')
        self.serializer = UserSerializer


    def test_create(self):

        user = self.serializer.create(self.serializer, self.user_values)
        self.assertEquals(user.email, self.user_values.get("email"))
        self.assertEquals(user.username, self.user_values.get("username"))
        self.assertEquals(user.phone_number, self.user_values.get("phone_number"))
        self.assertEquals(user.country, self.user_values.get("country"))
        self.assertEquals(user.city, self.user_values.get("city"))
        self.assertEquals(user.street_address, self.user_values.get("street_address"))



